exports.forEach=function(object, callback){
	if(typeof object!='object' || typeof callback!='function') return;
	var result={},
	key,
	value;
	var fnArgs=String(callback).split('){');
	fnArgs=fnArgs[0].split('(');
	fnArgs=fnArgs[1].split(',');
	if(object.constructor.name=='Object'){
		Object.keys(object).forEach(function(key){
			value=object[key];
			if(result.function===false){
				result.return=false;
				return false;
			}
			if(fnArgs.length==2) result.function=callback(key, value);
			else result.function=callback(value);
		});
	}
	else{
		for(key=0; key<object.length; key++){
			value=object[key];
			if(result.function===false){
				result.return=false;
				break;
			}
			if(fnArgs.length==2) result.function=callback(key, value);
			else result.function=callback(value);
		}
	}
	return result.return;
};