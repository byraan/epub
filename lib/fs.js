const path=require('path');
module.exports=require('fs');
module.exports._accessSync=module.exports.accessSync;
module.exports.accessSync=function(_path, _mode){
	try{
		module.exports._accessSync(_path, _mode);
		return true;
	}
	catch(err){
		return false;
	}
};