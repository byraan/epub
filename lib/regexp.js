module.exports={
	file: function(dirname, file, extension){
		if(dirname) dirname=dirname.replace(/([\$])/g, '\\\$1');
		else dirname='.*';
		if(!file || typeof file!='string') file='[\\w _ \\. \\- \$ \!]*';
		else file=file.replace(/([\$])/g, '\\\$1');
		if(typeof extension=='object' && extension.constructor.name=='Array'){
			let exts=([]).concat(extension);
			extension='';
			for(let i=0; i<exts.length; i++){
				if(exts[i].charAt(0)=='.') exts[i]=exts[i].slice(1);
				extension+='|'+exts[i];
			}
			extension=extension.slice(1);
			extension='('+extension+')';
		}
		var r=RegExp('^(?:'+dirname+').*\\/'+file+(typeof extension=='string'?'\\.'+extension:'')+'$');
		return r;
	}
};